#! /usr/bin/env node
const path = require("path");
//捕获cmd命令,主要是处理一些参数
const program = require("commander");
//cmd输出的美化工具
const chalk = require("chalk");
//命令行交互工具
const Inquirer = require("inquirer");
//艺术字
const figlet = require("figlet");
//操作文件
const fs = require("fs-extra");
//加载中样式
const ora = require("ora");
//git克隆（下载）
const clone = require("git-clone");

program
  .command("create <project-name>")
  .description("create a new project")
  .action(async function (projectName) {
    //获取当前目录
    const cwd = process.cwd();
    const targetDirectory = path.join(cwd, projectName);
    // console.log(targetDirectory);
    if (fs.existsSync(targetDirectory)) {
      const fugai = await new Inquirer.prompt([
        {
          name: "isOverwrite", //与返回值对应
          type: "list",
          message: "Target directory exists,Please choose an action",
          choices: [
            {
              name: "Overwrite",
              value: true,
            },
            {
              name: "Canvel",
              value: false,
            },
          ],
        },
      ]);
      if (!fugai.isOverwrite) {
        console.log("Canvel");
        return;
      } else {
        let spinner = ora("Remove...");
        spinner.start();
        fs.remove(targetDirectory);
        spinner.succeed("Already removed");
      }
    }
    //创建项目选择
    new Inquirer.prompt([
      {
        name: "nine",
        type: "checkbox",
        message: "Check the features needed for you project",
        choices: [
          {
            name: "Babel",
            checked: "true",
          },
          {
            name: "TypeScript",
          },
        ],
      },
    ]).then((data) => {
      //选择后输出
      const spinner = ora("Create project...");
      clone(
        "https://gitee.com/huang-also-has-a-story/android-small-project.git",
        projectName,
        { checkout: "main" },
        function (err) {
          spinner.start();
          setTimeout(() => {
          console.log(
            chalk.blue(
              "\r\n" +
                figlet.textSync("nine-cli", {
                  font: "Epic",
                  horizontalLayout: "default",
                  verticalLayout: "default",
                  width: 80,
                  whitespaceBreak: true,
                })
            )
          );
          spinner.succeed("Created successfully");
          console.log(chalk.green(`\n cd ${projectName}`));
            console.log(chalk.green(` npm run dev\n`));
          },3000)
        }
      );
    });
  });

program.on("--help", function () {
  console.log(
    figlet.textSync("help", {
      font: "3D-ASCII",
      horizontalLayout: "default",
      verticalLayout: "default",
      width: 80,
      whitespaceBreak: true,
    })
  );
});

program
  .name("nine-cli")
  .usage(`<command>[option]`)
  //查看版本-V
  .version(`v${require("../package.json").version}`);

program.parse(process.argv);
